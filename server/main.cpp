/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <getopt.h>
#include <sched.h>

#include <mvVISLAM.h>

#include <modal_pipe.h>
#include <voxl_common_config.h>
#include <cpu_monitor_interface.h>

#include <voxl_qvio_server.h>
#include "time.h"
#include "config_file.h"
#include "quality.h"
#include "overlay.h"

// server channels
#define EXTENDED_CH	0
#define SIMPLE_CH	1
#define OVERLAY_PIPE_CH	2

// client channels and config
#define IMU_CH	0
#define CAM_CH	1
#define GPS_CH	2
#define CPU_CH	3
#define IMU_PIPE_MIN_PIPE_SIZE (1  * 1024 * 1024) // give ourselves huge buffers
#define CAM_PIPE_SIZE (256 * 1024 * 1024) // give ourselves huge buffers
#define PROCESS_NAME	"qvio-server"

// after 300ms with no response, the health monitor thread assumes mvVISLAM
// has locked up while processing a frame and starts sending messages indicating
// a stall has occured with a failed state
#define STALL_TIMEOUT_NS 300000000

// auto restart if the system fails to init after 3 seconds
#define INIT_FAILURE_TIMEOUT_NS 3000000000

// do not check for blowups until 1 second after VIO claims to have initialized
#define BLOWUP_DETECT_TIMEOUT_NS 1000000000


#define SILENT_STD(x) {\
	FILE *silentfd = fopen("/dev/null","w");     \
	int savedstdoutfd = dup(STDOUT_FILENO);      \
	fflush(stdout);                              \
	dup2(fileno(silentfd), STDOUT_FILENO);       \
	x                                            \
	fflush(stdout);                              \
	fclose(silentfd);                            \
	dup2(savedstdoutfd, STDOUT_FILENO);          \
	close(savedstdoutfd);                        \
}


static int en_config_only = 0;
static int en_debug = 0;
static int en_debug_pos = 0;
static int en_debug_timing_cam = 0;
static int en_debug_timing_imu = 0;
static int en_debug_timing_pose = 0;
static int en_debug_crash = 0;
static int en_debug_gps = 0;
static pthread_t health_thread;
static int standby_active = 0;

// these are the last timestamps that have completely passed into mvvislam
// cam time is middle of frame. Also last pose to have been received from mvvislam
static volatile int64_t last_imu_timestamp_ns = 0;
static volatile int64_t last_cam_timestamp_ns = 0;
static volatile int64_t last_real_pose_timestamp_ns = 0;
static volatile int64_t last_sent_timestamp_ns = 0;

// state of imu and camera connections
static volatile int is_imu_connected = 0;
static volatile int is_cam_connected = 0;

// flag set to 1 on reset to indicate to the blowup detector not to check
// for blowups until after VIO actually initializes
static volatile int hard_reset_blowup_flag = 1;

// flag and time when a reset is requested to indicate to the init failure
// detection how long VIO has been trying to init
static volatile int init_failure_detector_reset_flag = 1;
static volatile int64_t time_of_last_reset = 0;
static volatile int last_state = VIO_STATE_FAILED;
static volatile int blowup_detector_flag = 0;
static volatile int64_t time_of_first_okay = 0;


static mvVISLAM* mv_vislam_ptr;
static int is_initialized = 0;
static int blank_counter = 0;
static int fade_counter = 0;
static int64_t last_time_alignment_ns = 0;
static int32_t last_frame_frame_id = 0;
static int64_t last_frame_timestamp_ns = 0;
static float tbc[3];
static float ombc[3];
static mvCameraConfiguration mv_cam_conf;

// mvVISLAM functions are not thread safe, protect all calls to the mvVISLAM
// API with this mutex
static pthread_mutex_t mv_mtx = PTHREAD_MUTEX_INITIALIZER;

// set any error codes here for publishing in the data structure in addition
// to errors that come from mvVISLAM
static uint32_t global_error_codes = 0;

// function prototypes
static void _publish(camera_image_metadata_t meta, uint8_t* img);
static int _hard_reset(void);


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints\n\
-g, --debug-gps             enable gps-specific debug prints\n\
-h, --help                  print this help message\n\
-i, --timing-imu            show timing prints for imu processing\n\
-p, --position              print position and rotation\n\
-s, --debug-crash           print lots of numbers to track down location of crashes\n\
-t, --timing-cam            enable timing prints for camera processing\n\
-u, --timing-pose           enable timing prints for pose calculation\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",			no_argument,		0, 'c'},
		{"debug",			no_argument,		0, 'd'},
		{"debug-gps",		no_argument,		0, 'g'},
		{"help",			no_argument,		0, 'h'},
		{"timing-imu",		no_argument,		0, 'i'},
		{"position",		no_argument,		0, 'p'},
		{"debug-crash",		no_argument,		0, 's'},
		{"timing-cam",		no_argument,		0, 't'},
		{"timing-pose",		no_argument,		0, 'u'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdghipstu", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			en_config_only = 1;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'g':
			en_debug_gps = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 'i':
			en_debug_timing_imu = 1;
			break;

		case 'p':
			en_debug_pos = 1;
			break;

		case 's':
			en_debug_crash = 1;
			break;

		case 't':
			en_debug_timing_cam = 1;
			break;

		case 'u':
			en_debug_timing_pose = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	printf("attempting to shut down cleanly\n");
	pipe_server_close_all();
	pipe_client_close_all();
	if(is_initialized){
		mvVISLAM_Deinitialize(mv_vislam_ptr);
		is_initialized = 0;
	}
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// pose data is published from the same thread that does the camera processing
// and pose estimation. That freezes, sometimes for over a second, during blowups
// so this thread exists to keep data coming out during that situation, warning
// consumers that there is an issue.
// this does NOT monitor for blowup criteria, that's done in the camera thread
// as soon as a new pose is calculated. This thread is to warn when that
// camera thread freezes.
static void* _health_thread_func(__attribute__((unused)) void* ctx)
{
	while(main_running){
		usleep(30000); // run about the same speed as the camera

		int64_t current_time = _apps_time_monotonic_ns();
		int64_t delay_ns = current_time - last_real_pose_timestamp_ns;

		if(init_failure_detector_reset_flag && is_imu_connected && is_cam_connected){
			uint64_t time_since_reset = current_time - time_of_last_reset;
			if(time_since_reset > INIT_FAILURE_TIMEOUT_NS){
				fprintf(stderr, "WARNING failed to init in time, trying again\n");
				_hard_reset();
				continue;
			}
		}

		// If last packet is recent enough, or we are chilling in standby mode
		// there is nothing to worry about.
		if(delay_ns < STALL_TIMEOUT_NS || standby_active) continue;

		// Everything after this sends failure packets, this inlcudes global
		// error codes indicating if we are waiting for cam or IMU data

		// flag that we've sent a packet with the current timestamp
		last_sent_timestamp_ns = current_time;

		ext_vio_data_t d;	// complete "extended" vio MPA packet
		vio_data_t s;		// simplified vio packet

		// simple lib modal pipe standard vio packet
		memset(&s,0,sizeof(s));
		s.magic_number = VIO_MAGIC_NUMBER;
		s.timestamp_ns = current_time;
		s.error_code = global_error_codes | ERROR_CODE_STALLED;
		s.state = VIO_STATE_FAILED;
		s.quality = -1.0f;

		// full extended qvio packet
		memset(&d,0,sizeof(d));
		d.v = s;

		// send to both pipes
		pipe_server_write(EXTENDED_CH, (char*)&d, sizeof(ext_vio_data_t));
		pipe_server_write(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));
		_draw_and_publish_vio_overlay(OVERLAY_PIPE_CH, NULL, NULL, d);

		// turn off dropped cam frame code now we have informed everyone.
		global_error_codes &= ~ERROR_CODE_DROPPED_CAM;
	}

	fprintf(stderr, "exiting health thread\n");
	return NULL;
}


static int _hard_reset(void)
{
	// lock the mutex before calling any mv api calls
	pthread_mutex_lock(&mv_mtx);

	// stop it if it's running
	if(is_initialized){
		is_initialized = 0;
		SILENT_STD(mvVISLAM_Deinitialize(mv_vislam_ptr);)
		mv_vislam_ptr = NULL;
	}

	// let the blowup detection know we just had a reset
	hard_reset_blowup_flag = 1;

	// let the init failure detector know we just had a reset
	init_failure_detector_reset_flag = 1;
	blowup_detector_flag = 0;
	last_state = VIO_STATE_FAILED;
	time_of_last_reset = _apps_time_monotonic_ns();

	// constant parameters
	float tba[3] = {0, 0, 0}; // position of gps relative to imu
	float logDepthBootstrap = log(1.0); // unused depth bootstrap

	// apply mask if present
	const char* staticMaskFileName = NULL;
	if(mask_file_path[0]!=0){
		staticMaskFileName = mask_file_path; // for image mask;
	}

	// now start again
	SILENT_STD(mv_vislam_ptr = mvVISLAM_Initialize(
		&mv_cam_conf,
		0, // global shutter
		tbc,
		ombc,
		cam_imu_timeshift_s,
		T_cam_wrt_imu_uncertainty,
		R_cam_to_imu_uncertainty,
		cam_imu_timeshift_s_uncertainty,
		accl_fsr_ms2,
		gyro_fsr_rad,
		accl_noise_std_dev,
		gyro_noise_std_dev,
		cam_noise_std_dev,
		min_std_pixel_noise,
		fail_high_pixel_noise_points,
		logDepthBootstrap,
		use_camera_height_bootstrap,
		log(camera_height_off_ground_m),
		!enable_init_while_moving,
		limited_imu_bw_trigger,
		staticMaskFileName,
		gps_imu_time_alignment_s,
		tba,
		enable_mapping);)

	pthread_mutex_unlock(&mv_mtx);

	if(mv_vislam_ptr == NULL){
		fprintf(stderr, "Error creating mvVISLAM object\n");
		_quit(-1);
	}

	is_initialized = 1;
	return 0;
}


// control listens for reset commands
static void _control_pipe_cb(__attribute__((unused)) int ch, char* string, \
							 int bytes, __attribute__((unused)) void* context)
{
	// remove the trailing newline from echo
	if(bytes>1 && string[bytes-1]=='\n'){
		string[bytes-1]=0;
	}

	if(strncmp(string, RESET_VIO_SOFT, strlen(RESET_VIO_SOFT))==0){
		printf("Client requested soft reset\n");
		pthread_mutex_lock(&mv_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 0);
		pthread_mutex_unlock(&mv_mtx);
		return;
	}
	if(strncmp(string, RESET_VIO_HARD, strlen(RESET_VIO_HARD))==0){
		printf("Client requested hard reset\n");
		// mvVISLAM_Reset is not reliable, just reinitialize the whole object instead
		// mvVISLAM_Reset(mv_vislam_ptr, 1);
		_hard_reset(); // close and restart the mvvislam object
		return;
	}
	if(strcmp(string, BLANK_VIO_CAM_COMMAND)==0){
		printf("Client requested image blank test\n");
		blank_counter = 90;
		return;
	}

	if(strcmp(string, FADE_VIO_CAM_COMMAND)==0){
		printf("Client requested image fade test\n");
		fade_counter = 255;
		return;
	}

	printf("WARNING: Server received unknown command through the control pipe!\n");
	printf("got %d bytes. Command is: %s\n", bytes, string);
	return;
}


static void _overlay_connect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to overlay\n", client_name);
	return;
}


static void _overlay_disconnect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" disconnected from overlay\n", client_name);
	return;
}


static void _cpu_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Connected to cpu-monitor\n");
	return;
}


static void _cpu_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "Disconnected from cpu-monitor\n");
	return;
}


// called whenever the simple helper has data for us to process
static void _cpu_helper_cb(__attribute__((unused))int ch, char* raw_data, int bytes, __attribute__((unused)) void* context)
{
	int n_packets;
	cpu_stats_t *data_array = modal_cpu_validate_pipe_data(raw_data, bytes, &n_packets);
	if (data_array == NULL) return;

	// only use most recent packet
	cpu_stats_t data = data_array[n_packets-1];

	if(data.flags&CPU_STATS_FLAG_STANDBY_ACTIVE){
		if(!standby_active){
			printf("Entering standby mode\n");
			standby_active = 1;
		}
	}
	else{
		if(standby_active){
			printf("Exiting standby mode\n");
			standby_active = 0;
		}
	}

	return;
}


// imu callback registered to the imu server
static void _imu_helper_cb(__attribute__((unused))int ch, char* data, int bytes,\
											__attribute__((unused)) void* context)
{
	if(en_debug_crash) fprintf(stderr, "0\n");
	// validate that the data makes sense
	int i, n_packets;
	imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(data_array == NULL) return;
	if(n_packets<=0) return;

	// flag that imu data is active, skip data if camera is disconnected
	is_imu_connected = 1;
	global_error_codes &= ~ERROR_CODE_IMU_MISSING;
	if(!is_cam_connected) return;
	if(!is_initialized) return;
	pthread_mutex_lock(&mv_mtx);

	// time this in debug mode
	int64_t time_before, process_time;
	if(en_debug_timing_imu) time_before = _apps_time_monotonic_ns();
	if(en_debug_crash) fprintf(stderr, "1\n");

	// add all data into VIO
	for(i=0; i<n_packets; i++){

		// offset for debugging
		// int64_t offset = 10000000;
		int64_t offset = 0;
		int64_t this_ts = data_array[i].timestamp_ns + offset;

		// check if we somehow got an out-of-order imu sample and reject it
		if(this_ts <= last_imu_timestamp_ns){
			double dt = (last_imu_timestamp_ns - this_ts)/1000000.0;
			fprintf(stderr, "WARNING out-of-order imu %fms before previous\n", dt);
			continue;
		}



		// pass data straight into mvvislam
		mvVISLAM_AddAccel(mv_vislam_ptr, this_ts, (double)data_array[i].accl_ms2[0],
									(double)data_array[i].accl_ms2[1], (double)data_array[i].accl_ms2[2]);
		mvVISLAM_AddGyro(mv_vislam_ptr, this_ts, (double)data_array[i].gyro_rad[0],
									(double)data_array[i].gyro_rad[1], (double)data_array[i].gyro_rad[2]);

		// record last timestamp so the camera thread is aware
		last_imu_timestamp_ns = this_ts;
	}

	pthread_mutex_unlock(&mv_mtx);
	if(en_debug_crash) fprintf(stderr, "2\n");

	if(en_debug_timing_imu){
		int64_t now_ns = _apps_time_monotonic_ns();
		process_time = now_ns - time_before;
		printf("IMU proc time: %6.2fms samples: %d latency: %6.2fms\n",\
			((double)process_time)/1000000.0, n_packets,\
			(time_before-data_array[n_packets-1].timestamp_ns)/1000000.0);
	}

	return;
}


// camera server should have triggered the imu read already, but if it didn't,
// or was somehow out of sync, we can use this to trigger the imu server to read
// the fifo buffer asynchronously if we don't have enough data yet.
// this should not normally be used
static void _send_imu_read_command(void)
{
	static int fd = 0;

	// file is closed, try to open it
	if(fd<=0){
		fd = open("/run/mpa/imu_apps/control", O_WRONLY|O_NONBLOCK);
	}

	if(fd<=0){
		// perror("ERROR opening control pipe");
		return;
	}

	int ret = write(fd, "read", 5);
	if(ret!=5){
		// perror("ERROR writing to control pipe");
		close(fd);
		fd = 0;
	}
	return;
}


// camera frame callback registered to voxl-camera-server
static void _cam_helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta,\
							char* frame, __attribute__((unused)) void* context)
{
	uint8_t* img;
	static int skipped_frames = 0;

	// check image format and grab correct img pointer from stereo if needed
	if(	meta.format == IMAGE_FORMAT_RAW8 ||\
		meta.format == IMAGE_FORMAT_NV12 ||\
		meta.format == IMAGE_FORMAT_STEREO_RAW8 ||\
		meta.format == IMAGE_FORMAT_STEREO_NV12)
	{
		global_error_codes &= ~ERROR_CODE_CAM_BAD_FORMAT;
		img = (uint8_t*)frame;
	}
	else{
		fprintf(stderr, "ERROR only support raw8 & nv12, mono & stereo right now\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_FORMAT;
		return;
	}

	// check resolution
	if(meta.width != (int16_t)mv_cam_conf.pixelWidth || meta.height != (int16_t)mv_cam_conf.pixelHeight){
		fprintf(stderr, "ERROR expected %dx%d img, got %dx%d\n",
					meta.width, meta.height, mv_cam_conf.pixelWidth, mv_cam_conf.pixelHeight);
		fprintf(stderr, "Image resolution must match what's in the selected lens cal file\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_RES;
		return;
	}
	else{
		global_error_codes &= ~ERROR_CODE_CAM_BAD_RES;
	}

	// Make the timestamp be the center of the exposure
	int64_t cam_timestamp_ns = meta.timestamp_ns;
	cam_timestamp_ns += meta.exposure_ns / 2;

	// flush the whole buffer if we are getting backed up
	if(cam_timestamp_ns < (_apps_time_monotonic_ns()-500000000)){
		global_error_codes |= ERROR_CODE_DROPPED_CAM;
		pipe_client_flush(CAM_CH);
		fprintf(stderr, "ERROR detected frame older than 0.5s, flushing cam pipe\n");
		return;
	}

	// flag that camera data is active, skip frame is imu is disconnected
	is_cam_connected = 1;
	global_error_codes &= ~ERROR_CODE_CAM_MISSING;
	if(!is_imu_connected) return;
	if(!is_initialized) return;

	// skip frames in standby mode
	if(standby_active && skipped_frames < standby_skip_frames){
		skipped_frames ++;
		return;
	}
	skipped_frames = 0;

	// debug arrival of imu data vs camera
	int64_t d = (cam_timestamp_ns + last_time_alignment_ns) - last_imu_timestamp_ns;
	double d_ms = d/1000000.0;
	if(d>0){
		if(en_debug_timing_imu) printf("alignment: %6.2fms, cam ts %5.1fms newer than last imu, triggering imu read\n", last_time_alignment_ns / 1000000.0, d_ms);
		_send_imu_read_command();
	}
	else{
		if(en_debug_timing_imu){
			printf("alignment: %6.2fms, cam ts %5.1fms older than last imu, great!!\n", last_time_alignment_ns / 1000000.0, -d_ms);
		}
	}

	// don't let image go in until IMU has caught up
	// if cam-imu alignment is POSITIVE that means the camera timestamp is early
	// and the image was actually taken after the reported timestamp
	while(last_imu_timestamp_ns < (cam_timestamp_ns + last_time_alignment_ns)){

		// don't get stuck here forever
		if(!main_running) return;
		if(!is_imu_connected) return;
		if(!is_initialized) return;
		if(cam_timestamp_ns < (_apps_time_monotonic_ns()-300000000)){
			global_error_codes |= ERROR_CODE_DROPPED_CAM;
			pipe_client_flush(CAM_CH);
			fprintf(stderr, "ERROR waited more than 0.3 seconds for imu to catch up, flushing camera pipe\n");
			return;
		}
		// small sleep if necessary
		if(en_debug){
			printf("waiting for imu\n");
		}
		usleep(5000);
	}


	// if fade counter is positive then fade out the image
	if(fade_counter>0){
		uint8_t tmp = 255 - fade_counter;
		for(int i=0; i<meta.size_bytes; i++){
			img[i]-=tmp;
			if(img[i]>fade_counter) img[i]=0; // fix wraparound
		}
		fade_counter-=2;
		if(fade_counter<=0) blank_counter = 120;
		if(en_debug) printf("fade out image, counter: %d\n", fade_counter);
	}

	// if blank counter is positive then blank out the image
	else if(blank_counter>0){
		memset(img, 0, meta.size_bytes);
		blank_counter--;
		if(en_debug) printf("blanked out image, counter: %d\n", blank_counter);
	}

	// don't continue if other things are still initializing
	if(!is_initialized) return;

	// lock the mv mutex, don't unlock until after getting the pose
	pthread_mutex_lock(&mv_mtx);
	if(en_debug_crash) fprintf(stderr, "3\n");
	int64_t time_before = _apps_time_monotonic_ns();
	mvVISLAM_AddImage(mv_vislam_ptr, cam_timestamp_ns , img);
	int64_t process_time = _apps_time_monotonic_ns() - time_before;
	if(en_debug_crash) fprintf(stderr, "4\n");

	// save details of the camera frame we just passed into mvvislam
	last_frame_frame_id = meta.frame_id;
	last_frame_timestamp_ns = meta.timestamp_ns;
	//printf("cam_timestamp_ns: %lld\n", cam_timestamp_ns);

	// record timestamp of camera frame AFTER it's been injested
	// this is the middle of the exposure, used for making sure imu data
	// is up to data before passing in next frame
	last_cam_timestamp_ns = cam_timestamp_ns;
	//pthread_mutex_unlock(&mv_mtx);

	if(process_time > 500000000){
		// if image processing took more than half a second, something went wrong
		// and we should drop frames
		global_error_codes |= ERROR_CODE_DROPPED_CAM;
		pipe_client_flush(CAM_CH);
		fprintf(stderr, "ERROR slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
		fprintf(stderr, "Flushing camera frames\n");
	}
	else if(process_time > 50000000){
		// warn if image processing was a little slow, this happens from time
		// to time and isn't a big deal
		fprintf(stderr, "WARNING slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
	}

	if(en_debug_timing_cam){
		printf("CAM proc time %6.2fms\n", ((double)process_time)/1000000.0);
	}


	_publish(meta, img);
	pthread_mutex_unlock(&mv_mtx);
	return;
}


#ifdef PLATFORM_QRB5165
	#include "slpi_gps_listener.h"
#endif

// return 0 if all is well, otherwise return the reason for blowup
static int _check_for_blowup(mvVISLAMPose p, int good_features)
{
	int64_t current_ts = p.time;
	static int64_t last_time_with_good_cov = 0;
	static int64_t last_time_with_enough_features = 0;

	// reset timers to current time after reset so we don't trip this during init
	if(hard_reset_blowup_flag){
		last_time_with_enough_features = current_ts;
		last_time_with_good_cov = current_ts;
		hard_reset_blowup_flag = 0;
	}

	// Now go through our 4 blowup criteria
	// max velocity check
	float vel = sqrtf(	(p.velocity[0]*p.velocity[0]) + \
						(p.velocity[1]*p.velocity[1]) + \
						(p.velocity[2]*p.velocity[2]));
	if(vel > auto_reset_max_velocity){
		fprintf(stderr, "WARNING auto-resetting due to exceeding max velocity of %4.1fm/s\n", (double)auto_reset_max_velocity);
		return ERROR_CODE_VEL_INST_CERT;
	}
	
	// get max velocity covariance
	float cov = p.errCovVelocity[0][0];
	if(p.errCovVelocity[1][1]>cov) cov = p.errCovVelocity[1][1];
	if(p.errCovVelocity[2][2]>cov) cov = p.errCovVelocity[2][2];

	// min feature timeout check
	if(good_features>auto_reset_min_features){
		last_time_with_enough_features = current_ts;
	}
	else{
		float tmp = (float)(current_ts - last_time_with_enough_features)/1000000000.0f;
		if(tmp>auto_reset_min_feature_timeout_s){
			fprintf(stderr, "WARNING auto-resetting due to low feature count\n");
			return ERROR_CODE_LOW_FEATURES;
		}
	}

	// max v cov timeout check
	if(cov<auto_reset_max_v_cov){
		last_time_with_good_cov = current_ts;
	}
	else{
		float tmp = (current_ts - last_time_with_good_cov)/1000000000.0f;
		if(tmp>auto_reset_max_v_cov_timeout_s){
			fprintf(stderr, "WARNING auto-resetting due to high vel covariance\n");
			return ERROR_CODE_VEL_WINDOW_CERT;
		}
	}

	// check for instant vel covariance limit
	if(cov  > auto_reset_max_v_cov_instant){
		fprintf(stderr, "WARNING auto-resetting due to vel covariance instant limit\n");
		return ERROR_CODE_VEL_INST_CERT;
	}

	// all is good (for now)
	return 0;
}


static void _publish(camera_image_metadata_t meta, uint8_t* img)
{
	mvVISLAMPose p;		// pose data from mvvislam
	ext_vio_data_t d;	// complete "extended" vio MPA packet
	vio_data_t s;		// simplified vio packet
	int nPoints;
	int n_good_points = 0;
	int n_oos_points = 0;
	int i,j;

	// make sure we start with clean data structs and apply any global error codes
	// full extended qvio packet, only needs memset as we memcpy the simple packet in later
	memset(&d,0,sizeof(d));
	// simple lib modal pipe standard vio packet
	memset(&s,0,sizeof(s));
	s.magic_number = VIO_MAGIC_NUMBER;
	s.error_code = global_error_codes;

	// Grab the pose and feature points
	if(en_debug_crash) fprintf(stderr, "7\n");
	int64_t time_before, process_time;
	if(en_debug_timing_pose) time_before = _apps_time_monotonic_ns();
	p = mvVISLAM_GetPose(mv_vislam_ptr);
	if(en_debug_crash) fprintf(stderr, "8\n");

	// get features and calculate the number of good features
	#define MAX_POINTS 256
	mvVISLAMMapPoint pPoints[MAX_POINTS];
	nPoints = mvVISLAM_GetPointCloud(mv_vislam_ptr, pPoints,MAX_POINTS);
	if(en_debug_timing_pose){
		process_time = _apps_time_monotonic_ns()-time_before;
		printf("getpose time = %5.2fms\n", process_time/1000000.0);
	}

	if(en_debug_crash) fprintf(stderr, "9\n");

	// record that we just got a successful pose and point cloud
	last_real_pose_timestamp_ns = p.time;

	for(i=0;i<nPoints;i++){
		if(pPoints[i].pointQuality==2 && pPoints[i].pixLoc[0]>0.0f && pPoints[i].pixLoc[1]>0.0f){
			n_good_points++;
		}
		if(pPoints[i].pointQuality == 1){
			n_oos_points++;
		}

		if (i < VIO_MAX_REPORTED_FEATURES){
			d.features[i].id = pPoints[i].id;
			d.features[i].cam_id = 0;
			memcpy(d.features[i].pix_loc, pPoints[i].pixLoc, sizeof(d.features[i].pix_loc));
			memcpy(d.features[i].tsf, pPoints[i].tsf, sizeof(d.features[i].tsf));
			memcpy(d.features[i].p_tsf, pPoints[i].p_tsf, sizeof(d.features[i].p_tsf));
			d.features[i].depth = pPoints[i].depth;
			d.features[i].depth_error_stddev = pPoints[i].depthErrorStdDev;
			d.features[i].point_quality = (vio_point_quality_t)pPoints[i].pointQuality;
			//printf("%2d %7.1f %7.1f %7.1f %7.1f\n", n_good_points, (double)pPoints[i].depth, (double)pPoints[i].depthErrorStdDev, d.features[i].pix_loc[0], d.features[i].pix_loc[1]);

		}
	}

	// limit the number of features to what fits in our pipe packet
	d.n_total_features = nPoints;
	if(d.n_total_features > VIO_MAX_REPORTED_FEATURES){
		d.n_total_features = VIO_MAX_REPORTED_FEATURES;
	}

	//if(en_debug) printf("total points: %3d  Good Points: %3d\n", nPoints, n_good_points);

	// grab state from mv
	if(p.poseQuality == MV_TRACKING_STATE_FAILED) s.state = VIO_STATE_FAILED;
	else if(p.poseQuality == MV_TRACKING_STATE_INITIALIZING) s.state = VIO_STATE_INITIALIZING;
	else s.state = VIO_STATE_OK;

	// sometimes qvio will report covariance as invalid but state is still OKAY
	// this is NOT alright, in this case manually set the state to failed.
	if(p.errCovPose[3][3]<=0.0f || p.errCovPose[4][4]<=0.0f || p.errCovPose[5][5]<=0.0f){
		s.state=VIO_STATE_FAILED;
	}

	// we finished initializing, no longer check for init timeout
	if(s.state == VIO_STATE_OK){
		init_failure_detector_reset_flag = 0;
	}

	// if we just went from good to failed, treat this like a reset for the
	// init failure detector so it can timeout the same as VIO tries to re-init
	// itself after it's own internal reset
	if(last_state!=VIO_STATE_FAILED && s.state==VIO_STATE_FAILED){
		init_failure_detector_reset_flag = 1;
		time_of_last_reset = p.time;
	}

	// record time when vio claimed to have initialized and only check for
	// for blowups some time after this
	if(last_state!=VIO_STATE_OK && s.state==VIO_STATE_OK && en_auto_reset){
		blowup_detector_flag = 1;
		time_of_first_okay = p.time;
	}
	last_state = s.state;

	// while VIO state is OK, do our own additional blowup checks if enough
	// time has passed since the init
	int64_t time_since_first_okay = _apps_time_monotonic_ns() - time_of_first_okay;
	if(blowup_detector_flag && time_since_first_okay>BLOWUP_DETECT_TIMEOUT_NS){
		int code = _check_for_blowup(p,n_good_points);
		if(code){
			pthread_mutex_unlock(&mv_mtx);
			_hard_reset();
			s.state = VIO_STATE_FAILED;
			s.error_code |= code;
		}
	}


	// don't send packets from the past, this can happen when qvio stalls
	// during a reset
	if(p.time<last_sent_timestamp_ns){
		//fprintf(stderr, "WARNING skipping pose data from the past\n");
		return;
	}

	// All checks passed, after this point this function should not return
	// until the end
	last_sent_timestamp_ns = p.time;


	// error code is a bitmask that we might have added to already so OR
	// it with VIOs error code
	s.error_code |= p.errorCode;

	// turn off dropped imu error for now, I think this error code is being
	// reported incorrectly by VIO since it's sportadic and seems to have no
	// correlation with IMU rate or phase with camera.
	s.error_code &= ~ERROR_CODE_DROPPED_IMU;

	// populate some other data
	s.timestamp_ns = p.time;
	s.n_feature_points = n_good_points;

	d.imu_cam_time_shift_s = p.timeAlignment;
	last_time_alignment_ns = p.timeAlignment * 1000000000;
	d.last_cam_frame_id = last_frame_frame_id;
	d.last_cam_timestamp_ns = last_frame_timestamp_ns;

	// deconstruct mv's 6DRT struct
	for(i=0;i<3;i++){
		s.T_imu_wrt_vio[i] = p.bodyPose.matrix[i][3];
		for(j=0;j<3;j++) s.R_imu_to_vio[i][j] = p.bodyPose.matrix[i][j];
	}

	// pose covariance diagonals, 6 entries
	s.pose_covariance[0] =  (float)p.errCovPose[0][0];
	s.pose_covariance[6] =  (float)p.errCovPose[1][1];
	s.pose_covariance[11] = (float)p.errCovPose[2][2];
	s.pose_covariance[15] = (float)p.errCovPose[3][3];
	s.pose_covariance[18] = (float)p.errCovPose[4][4];
	s.pose_covariance[20] = (float)p.errCovPose[5][5];

	// velocity covariance diagonals, 3 entries
	s.velocity_covariance[0] = p.errCovVelocity[0][0];
	s.velocity_covariance[6] = p.errCovVelocity[1][1];
	s.velocity_covariance[11] = p.errCovVelocity[2][2];

	// memcpy the rest
	memcpy(s.T_cam_wrt_imu,		p.tbc,				sizeof(float)*3);
	memcpy(s.R_cam_to_imu,		p.Rbc,				sizeof(float)*3*3);
	memcpy(s.vel_imu_wrt_vio,	p.velocity,			sizeof(float)*3);
	memcpy(s.imu_angular_vel,	p.angularVelocity,	sizeof(float)*3);
	memcpy(s.gravity_vector,	p.gravity,			sizeof(float)*3);
	
	memcpy(d.gravity_covariance,p.errCovGravity,	sizeof(float)*3*3);
	memcpy(d.gyro_bias,			p.wBias,			sizeof(float)*3);
	memcpy(d.accl_bias,			p.aBias,			sizeof(float)*3);

	// calculate the quality
	s.quality = calc_quality(s.state, s.velocity_covariance, mv_cam_conf.pixelWidth,\
					mv_cam_conf.pixelHeight, d.n_total_features, d.features);

	// fill the simplified struct in our extended packet
	memcpy(&d.v, &s, sizeof(vio_data_t));

	// send to both pipes
	pipe_server_write(EXTENDED_CH, (char*)&d, sizeof(ext_vio_data_t));
	pipe_server_write(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));

	// for debug only
	if(en_debug){
		printf("state: ");
		pipe_print_vio_state(s.state);
		printf(" err: ");
		pipe_print_vio_error(s.error_code);
		printf("\n");
	}
	if(en_debug_pos){
		printf("%6.3f %6.3f %6.3f ", (double)s.T_imu_wrt_vio[0],(double)s.T_imu_wrt_vio[1],(double)s.T_imu_wrt_vio[2]);
		printf("\n");
	}

	// turn off dropped frame code now we have informed everyone.
	global_error_codes &= ~ERROR_CODE_DROPPED_CAM;

	// if someone has subscribed to the overlay, draw it
	if(pipe_server_get_num_clients(OVERLAY_PIPE_CH) > 0){

		if(en_debug_crash) fprintf(stderr, "10\n");
		_draw_and_publish_vio_overlay(OVERLAY_PIPE_CH, &meta, (char*)img, d);
		if(en_debug_crash) fprintf(stderr, "11\n");
	}

	return;
}


// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _imu_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to imu server\n");
	pipe_client_set_pipe_size(IMU_CH, IMU_PIPE_MIN_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the imu server
static void _imu_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from imu server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_IMU_MISSING;
	last_imu_timestamp_ns = 0;
	is_imu_connected = 0;
	_hard_reset();
	return;
}

// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _cam_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to camera server\n");
	pipe_client_set_pipe_size(CAM_CH, CAM_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the camera server
static void _cam_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from camera server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_CAM_MISSING;
	last_cam_timestamp_ns = 0;
	is_cam_connected = 0;
	_hard_reset();
	return;
}


// main just reads config files, opens pipes, and waits
int main(int argc, char* argv[])
{
	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	////////////////////////////////////////////////////////////////////////////////
	// load config
	////////////////////////////////////////////////////////////////////////////////

	// start with the qvio config file as it contains imu/camera names
	printf("loading config file\n");
	if(config_file_read()) return -1;
	// in config only mode, just quit now
	if(en_config_only) return 0;
	// in normal mode print the config for logging purposes
	config_file_print();

	// load first enbaled vio cam from common vio-cam config file
	vio_cam_t vio_cam;
	if(vcc_read_vio_cam_conf_file(&vio_cam, 1, 1) !=1) return -1;
	printf("vio_cam config:\n");
	vcc_print_vio_cam_conf(&vio_cam, 1);
	printf("=================================================================\n");

	// check extrinsics and cam cal are present
	if(!vio_cam.is_extrinsic_present){
		fprintf(stderr, "failed to find extrinsic config\n");
		return -1;
	}
	if(!vio_cam.is_cal_present){
		fprintf(stderr, "failed to find cam cal\n");
		return -1;
	}

	////////////////////////////////////////////////////////////////////////////////
	// gracefully handle an existing instance of the process and associated PID file
	////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

	// set this critical process to use FIFO scheduler with high priority
	pipe_set_process_priority(THREAD_PRIORITY_RT_HIGH);

	// on qrb5165 keep this process on the larger cores
#ifdef PLATFORM_QRB5165
	set_cpu_affinity(cpu_set_big_cores_and_gold_core());
	print_cpu_affinity();
#endif

////////////////////////////////////////////////////////////////////////////////
// setup
////////////////////////////////////////////////////////////////////////////////

	// starting point for cam alignment comes from config file
	last_time_alignment_ns = cam_imu_timeshift_s * 1000000000;

	// now grab the extrinsic relation between the imu and camera
	for(int i=0;i<3;i++) tbc[i] = vio_cam.extrinsic.T_child_wrt_parent[i];
	vcc_tait_bryan_intrinsic_degrees_to_rotation_vector(vio_cam.extrinsic.RPY_parent_to_child, ombc);

	// these are default intrinsic lens constants for ModalAI Tracking camera
	mv_cam_conf.pixelWidth			= vio_cam.cal.width;
	mv_cam_conf.pixelHeight			= vio_cam.cal.height;
	mv_cam_conf.memoryStride		= vio_cam.cal.width;
	mv_cam_conf.uvOffset			= 0;
	mv_cam_conf.principalPoint[0]	= vio_cam.cal.cx;
	mv_cam_conf.principalPoint[1]	= vio_cam.cal.cy;
	mv_cam_conf.focalLength[0]		= vio_cam.cal.fx;
	mv_cam_conf.focalLength[1]		= vio_cam.cal.fy;
	mv_cam_conf.distortion[0]		= vio_cam.cal.D[0];
	mv_cam_conf.distortion[1]		= vio_cam.cal.D[1];
	mv_cam_conf.distortion[2]		= vio_cam.cal.D[2];
	mv_cam_conf.distortion[3]		= vio_cam.cal.D[3];
	mv_cam_conf.distortion[4]		= vio_cam.cal.D[4];
	mv_cam_conf.distortion[5]		= vio_cam.cal.D[5];
	mv_cam_conf.distortion[6]		= vio_cam.cal.D[6];
	mv_cam_conf.distortion[7]		= vio_cam.cal.D[7];
	mv_cam_conf.distortionModel		= 10; // fisheye
	if(!vio_cam.cal.is_fisheye) mv_cam_conf.distortionModel = 5; // pinhole model

	// start VIO with the reset function
	_hard_reset();


////////////////////////////////////////////////////////////////////////////////
// start the server pipe and data thread to write to it
////////////////////////////////////////////////////////////////////////////////

	int flags = SERVER_FLAG_EN_CONTROL_PIPE;

	// init extended pipe
	pipe_info_t info1 = { \
		QVIO_EXTENDED_NAME,			// name
		QVIO_EXTENDED_LOCATION,		// location
		"ext_vio_data_t",				// type
		PROCESS_NAME,				// server_name
		VIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(EXTENDED_CH, info1, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	cJSON* json = pipe_server_get_info_json_ptr(EXTENDED_CH);
	cJSON_AddStringToObject(json, "imu", vio_cam.imu );
	cJSON_AddStringToObject(json, "cam", vio_cam.name);
	pipe_server_update_info(EXTENDED_CH);
	pipe_server_set_control_cb(EXTENDED_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(EXTENDED_CH, QVIO_CONTROL_COMMANDS);


	// init simple pipe
	pipe_info_t info2 = { \
		QVIO_SIMPLE_NAME,			// name
		QVIO_SIMPLE_LOCATION,		// location
		"vio_data_t",				// type
		PROCESS_NAME,				// server_name
		VIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(SIMPLE_CH, info2, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	json = pipe_server_get_info_json_ptr(SIMPLE_CH);
	cJSON_AddStringToObject(json, "imu", vio_cam.imu );
	cJSON_AddStringToObject(json, "cam", vio_cam.name);
	pipe_server_update_info(SIMPLE_CH);
	pipe_server_set_control_cb(SIMPLE_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(SIMPLE_CH, QVIO_CONTROL_COMMANDS);


	// init overlay pipe
	pipe_info_t info3 = { \
		QVIO_OVERLAY_NAME,			// name
		QVIO_OVERLAY_LOCATION,		// location
		"camera_image_metadata_t",		// type
		PROCESS_NAME,				// server_name
		CAM_PIPE_SIZE,				// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(OVERLAY_PIPE_CH, info3, flags)){
		_quit(-1);
	}

	pipe_server_set_connect_cb(OVERLAY_PIPE_CH, _overlay_connect_cb, NULL);
	pipe_server_set_disconnect_cb(OVERLAY_PIPE_CH, _overlay_disconnect_cb, NULL);
	pipe_server_set_control_cb(OVERLAY_PIPE_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(OVERLAY_PIPE_CH, QVIO_CONTROL_COMMANDS);


	// indicate to the soon-to-be-started thread that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running = 1;

	// start health monitor thread
	pipe_pthread_create(&health_thread, _health_thread_func, NULL, THREAD_PRIORITY_RT_HIGH);

////////////////////////////////////////////////////////////////////////////////
// now subscribe to cam and imu servers, waiting if they are not alive yet
// VIO needs IMU before camera so init in that order.
////////////////////////////////////////////////////////////////////////////////

	// until they connect, inidcate that they are disconnected
	global_error_codes |= ERROR_CODE_CAM_MISSING;
	global_error_codes |= ERROR_CODE_IMU_MISSING;

	// try to open a pipe to the imu server
	pipe_client_set_connect_cb(IMU_CH, _imu_connect_cb, NULL);
	pipe_client_set_disconnect_cb(IMU_CH, _imu_disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(IMU_CH, _imu_helper_cb, NULL);
	printf("waiting for imu\n");
	if(pipe_client_open(IMU_CH, vio_cam.imu, PROCESS_NAME, \
			CLIENT_FLAG_EN_SIMPLE_HELPER, \
			IMU_RECOMMENDED_READ_BUF_SIZE))
	{
		fprintf(stderr, "Failed to open IMU pipe\n");
		_quit(-1);
	}

	// try to open pipe to camera server
	pipe_client_set_connect_cb(CAM_CH, _cam_connect_cb, NULL);
	pipe_client_set_disconnect_cb(CAM_CH, _cam_disconnect_cb, NULL);
	pipe_client_set_camera_helper_cb(CAM_CH, _cam_helper_cb, NULL);
	printf("waiting for cam\n");
	if(pipe_client_open(CAM_CH, vio_cam.pipe_for_tracking, PROCESS_NAME, \
				CLIENT_FLAG_EN_CAMERA_HELPER, 0))
	{
		fprintf(stderr, "Failed to open CAM pipe\n");
		_quit(-1);
	}

	// open optional GPS pipe
#ifdef PLATFORM_QRB5165
	if(enable_gps_vel){
		_slpi_gps_reader_init();
	}
#endif

	// open optional cpu monitor pipe
	if(en_standby_mode){
		pipe_client_set_connect_cb(CPU_CH, _cpu_connect_cb, NULL);
		pipe_client_set_disconnect_cb(CPU_CH, _cpu_disconnect_cb, NULL);
		pipe_client_set_simple_helper_cb(CPU_CH, _cpu_helper_cb, NULL);
		printf("waiting for cpu_monitor\n");
		if(pipe_client_open(CPU_CH, "cpu_monitor", PROCESS_NAME, \
				CLIENT_FLAG_EN_SIMPLE_HELPER, \
				CPU_STATS_RECOMMENDED_READ_BUF_SIZE))
		{
			fprintf(stderr, "Failed to open CPU pipe\n");
			_quit(-1);
		}
	}

	// run until start/stop module catches a signal and changes main_running to 0
	while(main_running) usleep(5000000);


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	printf("joining health thread\n");
	pthread_join(health_thread, NULL);

	#ifdef PLATFORM_QRB5165
	if(enable_gps_vel){
		_slpi_gps_reader_stop();
	}
	#endif
	_quit(0);
	return 0;
}
