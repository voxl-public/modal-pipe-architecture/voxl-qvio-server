#!/bin/bash
################################################################################
# Copyright 2024 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

NAME="voxl-qvio-server"
SERVICE_FILE="${NAME}.service"
CONFIG_FILE="/etc/modalai/${NAME}.conf"
USER=$(whoami)


print_usage () {
	echo ""
	echo "voxl-configure-qvio"
	echo ""
	echo "factory_enable will reset the config file to factory defaults"
	echo "before enabling the service."
	echo ""
	echo "voxl-configure-qvio disable"
	echo "voxl-configure-qvio factory_enable"
	echo "voxl-configure-qvio enable"
	echo ""
	echo "qvio pulls camera and imu info from /etc/modalai/vio_cams.conf"
	echo "set that file up separately with voxl-configure-vio-cams"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-qvio help"
	echo ""
}


## set most parameters which don't have quotes in json
set_param () {
	if [ "$#" != "2" ]; then
		echo "set_param expected 2 args"
		exit 1
	fi

	# remove quotes if they exist
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	val=$2
	val="${val%\"}"
	val="${val#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	$val," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_param_string () {
	if [ "$#" != "2" ]; then
		echo "set_param_string expected 2 args"
		exit 1
	fi
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	sed -E -i "/\"$var\":/c\	\"$var\":	\"$2\"," ${CONFIG_FILE}
}


disable_service_and_exit () {
	echo "disabling ${NAME} systemd service"
	systemctl disable ${SERVICE_FILE}
	echo "stopping ${NAME} systemd service"
	systemctl stop ${SERVICE_FILE}
	echo "Done configuring ${NAME}"
	exit 0
}

enable_service_and_exit () {
	echo "enabling  ${NAME} systemd service"
	systemctl enable  ${SERVICE_FILE}
	echo "Done configuring ${NAME}"
	exit 0
}

reset_config_file_to_default () {
	echo "wiping old config file"
	rm -rf ${CONFIG_FILE}
	${NAME} -c
}


################################################################################
## actual start of execution, handle optional arguments first
################################################################################

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi


## parse arguments
case $1 in
	"")
		echo "no argument given"
		print_usage
		exit 1
		;;
	"h"|"-h"|"help"|"--help")
		print_usage
		exit 0
		;;
	"enable")
		enable_service_and_exit
		;;
	"disable")
		disable_service_and_exit
		;;
	"factory_enable")
		reset_config_file_to_default
		enable_service_and_exit
		;;

	"factory_enable_imu0"|\
	"factory_enable_imu1"|\
	"factory_enable_imu_px4"| \
	"factory_enable_imu_apps"|\
	"factory_enable_imu_apps_stereo_lower"|\
	"factory_enable_imu_apps_trackingL"|\
	"factory_enable_imu_apps_tracking_front"|\
	"factory_enable_imu_apps_tracking_down")


		echo ""
		print_usage
		echo ""
		echo "ERROR you are using an old deprecated config mode"
		echo "vio imu and cameras are now configured separately"
		echo "with voxl-configure-vio-cams"
		exit 1
		;;

	*)
		echo "invalid option"
		print_usage
		exit 1
esac



# should not get here
exit 1
